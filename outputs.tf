#-----
# MAIN
#-----

output "region" {
  value = "${var.region}"
}

output "account_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

output "user_id" {
  value = "${data.aws_caller_identity.current.user_id}"
}

output "arn" {
  value = "${data.aws_caller_identity.current.arn}"
}

#---------
# ROUTE 53
#---------

output "zone_id" {
  description = "The Hosted Zone ID. This can be referenced by zone records."

  value = "${aws_route53_zone.this.zone_id}"
}

output "zone_name_servers" {
  description = "A list of name servers in associated (or default) delegation set. Find more about delegation sets in AWS docs."

  value = "${aws_route53_zone.this.name_servers}"
}

output "zone_name" {
  description = "(Required) This is the name of the hosted zone."

  value = "${aws_route53_zone.this.name}"
}

output "zone_comment" {
  description = "(Optional) A comment for the hosted zone. Defaults to 'Managed by Terraform'."

  value = "${aws_route53_zone.this.comment}"
}

output "zone_force_destroy" {
  description = "(Optional) Whether to destroy all records (possibly managed outside of Terraform) in the zone when destroying the zone."

  value = "${aws_route53_zone.this.force_destroy}"
}

output "zone_tags" {
  description = "(Optional) A mapping of tags to assign to the zone."

  value = "${aws_route53_zone.this.tags}"
}

output "zone_vpc" {
  description = "(Optional) Configuration block(s) specifying VPC(s) to associate with a private hosted zone. Conflicts with the delegation_set_id argument in this resource and any aws_route53_zone_association resource specifying the same zone ID. Detailed below."

  value = "${aws_route53_zone.this.vpc}"
}

output "record_zone_id" {
  description = "(Required) The ID of the hosted zone to contain this record."

  value = "${aws_route53_record.this.zone_id}"
}

output "record_name" {
  description = "(Required) The name of the record."

  value = "${aws_route53_record.this.name}"
}

output "record_type" {
  description = "(Required) The record type. Valid values are A, AAAA, CAA, CNAME, MX, NAPTR, NS, PTR, SOA, SPF, SRV and TXT."

  value = "${aws_route53_record.this.type}"
}

output "record_ttl" {
  description = "(Required for non-alias records) The TTL of the record."

  value = "${aws_route53_record.this.ttl}"
}

output "record_records" {
  description = "(Required for non-alias records) A string list of records. "

  value = "${aws_route53_record.this.records}"
}

output "record_fqdn" {
  description = "FQDN built using the zone domain and name."
  value       = "${aws_route53_record.this.fqdn}"
}
