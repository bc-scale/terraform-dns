provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"

  backend "s3" {}
}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    region         = "${var.region}"
    bucket         = "sk-prd-terraform-state"
    key            = "terraform-vpc/${var.environment}/terraform.state"
    encrypt        = "true"
    dynamodb_table = "sk-prd-terraform-state-lock"
  }
}

data "aws_route53_zone" "main" {
  name = "${var.main_domain}."
}

locals {
  tags   = "${merge(var.tags, map("Environment", "${var.environment}"))}"
  domain = "${var.stage}.${var.main_domain}"
}

module "route53_label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git"
  version    = "0.6.3"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["dns"]
  delimiter  = "-"
  tags       = "${local.tags}"
}

resource "aws_route53_zone" "this" {
  name = "${local.domain}"

  vpc {
    vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  }

  tags = "${module.route53_label.tags}"
}

resource "aws_route53_record" "this" {
  zone_id = "${data.aws_route53_zone.main.zone_id}"
  name    = "${local.domain}"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.this.name_servers.0}",
    "${aws_route53_zone.this.name_servers.1}",
    "${aws_route53_zone.this.name_servers.2}",
    "${aws_route53_zone.this.name_servers.3}",
  ]
}
